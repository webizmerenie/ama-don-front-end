###
* scripts from page contacts
*
* @author Andrey Chechkin
* @license GNU/AGPLv3
* @see {@link https://www.gnu.org/licenses/agpl-3.0.txt|License}
###

$ = require "jquery"

init = ->
	$main = $ ".contacts .contacts-list"
	$employeesList = $main.find ".employees-list > ul"

	if $main.length != 0
		$employeesList.height((($employeesList.height() / 2) + 20 ));

		$.getScript "https://api-maps.yandex.ru/2.1/?lang=ru_RU", (data, textStatus) ->
			if textStatus == "success"

				addMap = (idMap, centerMap, centerBaloon, textHover, srcBaloon)->
					centerMapArr = centerMap.split ","
					centerBaloonArr = centerBaloon.split ","

					ymaps.ready ->
						map = new ymaps.Map idMap,
							center: centerMapArr
							zoom: 16
							controls: ["zoomControl", "typeSelector"]

						map.behaviors.disable('scrollZoom')

						placemark = new ymaps.Placemark centerBaloonArr,
							{
								hintContent: textHover
							},
							{
								iconLayout: 'default#image'
								iconImageHref: srcBaloon
								iconImageSize: [73, 105]
								iconImageOffset: [-36.5, -105]
							}

						map.geoObjects.add(placemark);

				$maps = $main.find ".map"

				$maps.each ->
					centerM = $(@).data "center"
					centerB = $(@).data "baloon"
					text = $(@).data "baloon-text"
					id = $(@).attr "id"
					srBaloon = $(@).find("img").attr("src")

					addMap(id, centerM, centerB, text, srBaloon)
module.exports = { init }

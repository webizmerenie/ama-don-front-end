###
* main file scripts
*
* @author Andrey Chechkin
* @license GNU/AGPLv3
* @see {@link https://www.gnu.org/licenses/agpl-3.0.txt|License}
###

$ = require "jquery"

init = ->

	$callWindow = $ ".call-window"
	$overlay = $ ".overlay"
	$popup = $ ".popup"
	$close = $popup.find ".close"

	closeWindow = ->
		do $overlay.hide
		$popup.fadeOut(300, -> $(@).removeClass "sucsess")

	$callWindow.click (e) ->
		target = $(@).attr "href"
		do e.preventDefault

		do $overlay.show
		$(target).fadeIn(300)

	$overlay.click ->
		do closeWindow

	$close.click ->
		do closeWindow

module.exports = { init }

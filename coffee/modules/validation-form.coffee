###
* main file scripts
*
* @author Andrey Chechkin
* @license GNU/AGPLv3
* @see {@link https://www.gnu.org/licenses/agpl-3.0.txt|License}
###

$ = require "jquery"

module.exports = (form) ->
	$(form).find("input[name='q']").hide()
	$popup = $(form).closest ".popup"
	$close = $popup.find ".close"
	$input = $(form).find '.require'

	resetError = ->
		$input.removeClass "error"
		$(form).find(".error-msg").remove()

	$close.click ->
		$(form).trigger("reset")
		do resetError

	$(form).submit (e) ->
		error = false
		errorMsg = "<span class='error-msg'>Заполните это поле</span>"
		bot = $(@).find "input[name='q']"

		if(bot.val().length > 0)
			console.log "Go away fuckin bot!"
			return false

		do resetError

		$input.each ->
			$(@).val '' if $(@).val().replace(/\s/g, '') == ''
			if $(@).val() == ''
				error = true
				$(@).addClass "error"
				$(@).after(errorMsg)

		if error
			do e.preventDefault
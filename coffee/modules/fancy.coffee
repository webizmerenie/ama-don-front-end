###
* fancybox init
*
* @author Andrey Chechkin
* @license GNU/AGPLv3
* @see {@link https://www.gnu.org/licenses/agpl-3.0.txt|License}
###

$ = require "jquery"
fancybox = require "fancybox"

init = ->
	$(".fancy").fancybox
		padding: 0,
		helpers:
			overlay:
				locked: false

module.exports = { init }

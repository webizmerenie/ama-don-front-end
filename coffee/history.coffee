###
* Scripts from page history
*
* @author Andrey Chechkin
* @license GNU/AGPLv3
* @see {@link https://www.gnu.org/licenses/agpl-3.0.txt|License}
###

$ = require "jquery"
slick = require "slick"

init = ->
	$main = $ ".history body main"
	$historySlide = $main.find ".history-slide"
	$slide = $historySlide.find ".content ul"
	$navigation = $historySlide.find ".navigation"
	$navigationUl = $navigation.find "ul"
	$navigationItem = $navigationUl.find "li"
	navigationItemsWidth = 0

	$slide.slick
		speed: 1000
		infinite: false
		variableWidth: true
		focusOnSelect: true

	$($navigationItem[0]).addClass "active"

	$navigationItem.each ->
		navigationItemsWidth += $(@).width()

	syncSlider = (targetElement) ->
		$navigationItem.removeClass "active"
		$(targetElement).addClass "active"

		if navigationItemsWidth > $navigation.width()
			$prev = $(targetElement).prevAll()
			leftP = 0

			if $prev.length > 0
					$prev.each ->
						leftP += $(@).width()

					leftP -= $(targetElement).width() / 2
			$navigationUl.animate
				left: -leftP
				500

	$navigationItem.click ->
		if !$(@).hasClass "active"
			slideItem = $(@).data "slide"

			$slide.slick("slickGoTo", slideItem, false)
			syncSlider(@)

	$buttonSlide = $slide.find ".slick-arrow"

	$slide.on "beforeChange", (event, slick, currentSlide, nextSlide) ->
		syncSlider($navigationItem[nextSlide])
		
module.exports = { init }

###
* main file scripts
*
* @author Andrey Chechkin
* @license GNU/AGPLv3
* @see {@link https://www.gnu.org/licenses/agpl-3.0.txt|License}
###

$ = require "jquery"

#main-page
topmenu = require "./main-page/topmenu"
baner = require "./main-page/baner"
about = require "./main-page/about"
production = require "./production"
popup = require "./modules/popup"
validationForm = require "./modules/validation-form"
contacts = require "./contacts"
history = require "./history"
fancy = require "./modules/fancy"
factory = require "./factory"

$ ->
	$formQuest = $ "#form-quest form"

	do topmenu.init
	do baner.init
	do about.init
	do production.init
	do popup.init
	do history.init
	do fancy.init

	validationForm($formQuest)

window.onload = ->
	do contacts.init
	do factory.init

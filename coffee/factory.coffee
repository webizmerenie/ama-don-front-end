###
* scripts from page factory
*
* @author Andrey Chechkin
* @license GNU/AGPLv3
* @see {@link https://www.gnu.org/licenses/agpl-3.0.txt|License}
###

$ = require "jquery"

init = ->
	$main = $ ".factory main"
	if $main.length != 0
		$videoContainer = $main.find ".video"
		$video = $videoContainer.find "video"
		$playing = $videoContainer.find "video"
		$play = $videoContainer.find ".play"
		pauseTime = 0

		$play.click ->
			$video[0].currentTime = pauseTime
			do $video[0].play
			$videoContainer.addClass "playing"

		$playing.click ->
			$this = $(@)[0]

			pauseTime = $this.currentTime
			do $this.pause
			do $this.load
			$videoContainer.removeClass "playing"

		$video[0].onended = ->
			pauseTime = 0
			do $(@)[0].load
			$videoContainer.removeClass "playing"

		$video.find("source").on "error", ->
			alert "Ошибка загрузки видео"

module.exports = { init }

###
* scripts from topmenu
*
* @author Andrey Chechkin
* @license GNU/AGPLv3
* @see {@link https://www.gnu.org/licenses/agpl-3.0.txt|License}
###

$ = require "jquery"
slick = require "slick"

init = ->
	$main = $ ".main-page"

	if($main.length > 0)
		$baner = $main.find ".banner"
		$imgBanner = $baner.find ".image"
		$down = $main.find ".down"
		resizeBanner = ->
			w = $(window).width()
			h = $(window).height()

			$imgBanner.width(w)
			$imgBanner.height(h)

		do resizeBanner

		$(window).resize ->
			do resizeBanner

		$down.click (e) ->
			scrollElementPosition =  $baner.next().offset().top

			do e.preventDefault
			$('html, body').animate
				scrollTop: scrollElementPosition,
				500

		$imgBanner.slick
			infinite: true,
			autoplay: true,
			autoplaySpeed: 5000,
			fade: true,
			speed: 2000
			cssEase: 'linear',
			arrows: false,
			lazyLoad: 'ondemand'

		$imgBanner.find("img:first").on "load", ->
			$preloadBaner = $baner.find ".preload"
			do $preloadBaner.fadeOut

		$textBlock = $baner.find ".text"
		textBlockTop = $textBlock.offset().top + ($textBlock.height() / 2)

		$(window).scroll ->
			scrollTop = $(@).scrollTop()
			moved = (textBlockTop - (scrollTop * 0.5))
			$textBlock.css
				"top": "#{moved}px"

module.exports = { init }

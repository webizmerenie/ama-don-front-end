###
* scripts from topmenu
*
* @author Andrey Chechkin
* @license GNU/AGPLv3
* @see {@link https://www.gnu.org/licenses/agpl-3.0.txt|License}
###

$ = require "jquery"

init = ->
	$main = $ "header"
	$subitems = $ ".subitems"
	$submenus = $main.find ".submenu"

	calculationPosition = ->
		windowWidth = $(window).width()
		headerWidth = $("header").outerWidth()
		subitemsLeft = -(windowWidth - headerWidth) / 2
		$subitems.width(windowWidth)
		$subitems.css
			"left": subitemsLeft

	do calculationPosition

	$(window).resize(->
		do calculationPosition
	)

	$submenus.hover(
		->
			$(@).addClass("active")
		->
			$(@).removeClass("active")
	)

module.exports = { init }

###
* scripts from topmenu
*
* @author Andrey Chechkin
* @license GNU/AGPLv3
* @see {@link https://www.gnu.org/licenses/agpl-3.0.txt|License}
###

$ = require "jquery"

init = ->
	$main = $ ".main-page"
	$about = $main.find ".about"
	$aboutItems = $about.find "li"

	if($main.length != 0)
		showElements = ->
			if $(window).scrollTop() + $(window).height() < $about.offset().top
				$aboutItems.removeClass "active"

			$aboutItems.each ->
				scrollHeight = $(@).offset().top - $(@).height() * 2

				if(scrollHeight <= 0)
					$(@).addClass "active"

					if($about.find(".active").length % 2 > 0)
						$(@).next().addClass "active"
						$(@).prev().addClass "active"

		do showElements

		curScroll = 0
		$(document).scroll ->
			nextScroll = $(@).scrollTop()

			if(nextScroll > curScroll)
				do showElements

			curScroll = nextScroll

module.exports = { init }

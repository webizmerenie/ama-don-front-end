###
* scripts from topmenu
*
* @author Andrey Chechkin
* @license GNU/AGPLv3
* @see {@link https://www.gnu.org/licenses/agpl-3.0.txt|License}
###

$ = require "jquery"
slick = require "slick"
validation = require "./modules/validation-form"

init = ->
	$main = $ ".production body .content"
	$gallery = $main.find ".gallery"
	$galleryItems = $gallery.find "ul"

	$galleryItems.slick
		speed: 1000

	validation("#order")

	$constructor = $main.find ".constructor"

	$colorWires = $constructor.find ".color-wires a"
	$colorPlaques = $constructor.find ".color-plaques a"

	$formOrder = $constructor.find "#order"
	$inpGroupWires = $formOrder.find ".wires"
	$inpGroupPlaques = $formOrder.find ".plaques"
	$inpCheckboxes = $formOrder.find "input[type='checkbox']"

	$itemsLiptogapthy = $constructor.find ".choise-liptograpthy a"
	$renderImg = $constructor.find ".render-img img"

	dir = $itemsLiptogapthy.parent().find(".active").data("dir")
	wires = $colorWires.parent().find(".active").attr("href")
	plaques = $colorPlaques.parent().find(".active").attr("href")

	toggleActive = (parent, thisElement, cb) ->
		if(!$(thisElement).hasClass "active")
			parent.removeClass "active"
			$(thisElement).addClass "active"
			if cb?
				cb.call(thisElement)

	setColor = (target, formInp) ->
		target.click (e) ->
			do e.preventDefault

			toggleActive(target, @, () ->
				color = $(@).data "color"
				val = $(@).attr "href"
				$spanColor = formInp.find ".color"
				$inpColor = formInp.find "input"

				$spanColor.css
					background: color

				$inpCheckboxes.removeAttr "checked"

				if(target == $colorWires)
					wires = val
					$inpColor.val(val)
				else
					plaques = val
					if($(@).parent().hasClass("choise-liptograpthy"))
						$spanColor.css
							background: "none"
						$inpColor.val("")
						$formOrder.find(color).attr("checked", "checked")
					else
						$inpColor.val(val)
			)

	setColor($colorWires, $inpGroupWires)
	setColor($colorPlaques, $inpGroupPlaques)

	$constructor.on "click", ".color-wires a, .color-plaques a", ->
		path = "/upload/constructor/#{wires}-#{plaques}.png"

		$renderImg.animate
			opacity: 0
			400
			->
				$renderImg.attr("src", path)
					.on "load", ->
						$(@).animate
							opacity: 1
							400
					.on "error", -> console.log("Ошибка загрузки #{path}")

module.exports = { init }

# ama-don #

## Install ##

* git clone git@bitbucket.org:webizmerenie/ama-don-front-end.git
* cd ama-don-front-end
* npm i

## Build files ##

```
#!bash

gulp
```


## Start server ##

```
#!bash

gulp watch
```


### Author ###
**Andrey Chechkin** [andrey.chechkin@web-izmerenie.ru](mailto:andrey.chechkin@web-izmerenie.ru)